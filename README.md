# Flutter to do App UI "To Do List"

```dart
class Revaldo extends FlutterChallenge {
    return {
      "title": "Flutter to do App UI",
      "description": "Build a to do app UI with flutter",
      "Design": "https://www.uplabs.com/posts/to-do-list-app-freebie-kit"
    }
}
```

## Getting Started 🚀

```shell
- Clone the repo
- flutter pub get
- flutter run
```

## ScreenShot


